# Maintainer: Jan Cholasta <grubber at grubber cz>
# Contributor: Xiao-Long Chen <chenxiaolong@cxl.epac.to>

pkgbase=freeipa
pkgname=(
  python-ipapython
  python-ipaplatform
  python-ipalib
  python-ipaclient
  python-ipaserver
  freeipa-server
  freeipa-client)
pkgver=4.7.2
pkgrel=3
_pkgdesc_base='The Identity, Policy and Audit system'
pkgdesc="$_pkgdesc_base"
arch=('i686' 'x86_64')
url='http://www.freeipa.org/'
license=('GPL3')
makedepends=('openldap'
             'krb5>=1.15.1'
             'xmlrpc-c>=1.27.4'
             'popt'
             'gettext'
             'python'
             'python-setuptools'
             'python2'
             'python2-setuptools'
             'nspr'
             'nss'
             'openssl'
             'ding-libs'
             'libsasl'
             'python-ldap'
             'python-nss'
             'python2-netaddr>=0.7.16'
             'python-pyasn1'
             'python-pyasn1-modules'
             'python-dnspython'
             'python-six'
             'sssd>=1.13.0'
             'python2-cffi'
             'python-jinja'
             'python-pyasn1-modules'
             'python2-jinja'
	     'uglify-js'
	     'python-lesscpy'
            )
options=(emptydirs)
source=("https://releases.pagure.org/freeipa/freeipa-${pkgver}.tar.gz"
        0001-platform-add-Arch-Linux-platform.patch
        samba_headers_fix.patch::https://github.com/freeipa/freeipa/commit/d1f5ed64e16d65b9df45cc0eac7d2724dcae7b67.diff
	zero_struct_remove.patch::https://github.com/freeipa/freeipa/commit/272837f1c07729392cdbc88b99a221390d01e70d.diff
	lesscpy.patch::https://github.com/freeipa/freeipa/commit/27dcb57ac20694f1017ad95de292b508b7a2e7c0.diff
        lib389_1_4.patch::https://patch-diff.githubusercontent.com/raw/freeipa/freeipa/pull/2312.diff
        freeipa-client-update-sshd_config
        freeipa-client-update-sshd_config.hook
        freeipa.conf.sysusers
       )
md5sums=('dddf80fd3de85fa537f058a797b97d70'
         'e87a6b77cc63bd31efea44341ba6ae45'
         '4dd6183e4f24b5a433370d3f0b01d317'
         'b3b97e51c83009129736870c4b0be9a4'
         '08055e6fbfce9fa0e9f431958bd1b199'
         'fee8452c9f87475137814a6d4025bc9d'
         '0a0620da8922f8bcde73f1881ac210c4'
         'ce8953bf991cdd469ff3c68ddc468f13'
         '05b09eaac730d6f486ea71e863958a55')


prepare() {
  cd freeipa-${pkgver}

  rm -rf ipaplatform/arch

  patch -p1 -i"$srcdir"/0001-platform-add-Arch-Linux-platform.patch
  patch -p1 -i"$srcdir"/samba_headers_fix.patch
  patch -p1 -i"$srcdir"/zero_struct_remove.patch
  patch -p1 -i"$srcdir"/lesscpy.patch
  patch -p1 -i"$srcdir"/lib389_1_4.patch
}

build() {
  cd freeipa-${pkgver}

  #    export PYTHON=/usr/bin/python2     #   -exec sed -i -e '1 s|^#!.*\bpython[^ ]*|#!/usr/bin/python2|' {} \;

  export LIBVERTO_LIBS=-lverto
  export LIBVERTO_CFLAGS=-I/usr/include

  
  ./configure --prefix=/usr \
              --sysconfdir=/etc \
              --sbindir=/usr/bin \
              --localstatedir=/var \
              --with-vendor-suffix=-arch-${pkgrel} \
              --libexecdir=/usr/lib/"$pkgbase" \
              --enable-server \
              --without-ipatests \
              --disable-pylint --without-jslint

  make

  find -wholename '*/site-packages/*/install_files.txt' -exec rm {} \;


  #   (cd ipaclient && make install DESTDIR=../../install)
  #  (cd ipalib && make install DESTDIR=../../install)
  # (cd ipaplatform && make install DESTDIR=../../install)
  #(cd ipapython && make install DESTDIR=../../install)
  #popd

  # Python 2 installation
  #   make install DESTDIR="$PWD"/../install
  #
  # remove files which are useful only for make uninstall
  #  find ../install -wholename '*/site-packages/*/install_files.txt' -exec rm {} \;

  # mkdir -p ../install/etc/ipa
  #mkdir -p ../install/etc/ipa/nssdb
  #mkdir -p ../install/var/lib/ipa-client/pki
  #mkdir -p ../install/var/lib/ipa-client/sysrestore

  #touch ../install/etc/ipa/default.conf
  #touch ../install/etc/ipa/ca.crt
}

package_python-ipaserver()
{
  arch=('any')
  pkgdesc="$_pkgname python server libs"
  depends=(
            "python-cryptography"
            "python-custodia"
            "python-dbus"
            "python-dnspython"
            "python-dogtag-pki"
            "python-ipaclient"
            "python-ipalib"
            "python-ipaplatform"
            "python-ipapython"
            "python-jwcrypto"
            "python-lxml"
            "python-netaddr"
            "python-pyasn1"
            "python-requests"
            "python-six"
  )
  
  cd freeipa-${pkgver}/ipaserver
  make install DESTDIR="$pkgdir"
}

package_python-ipapython()
{
  arch=('any')
  depends=(
    "python-cffi"
    "python-cryptography"
    "python-dnspython"
    "python-gssapi"
    # "ipalib",  # circular dependency
    "python-ipaplatform"
    "python-netaddr"
    "python-netifaces"
    "python-ldap"
    "python-six"
    "python-dbus"
  )
  cd freeipa-${pkgver}/ipapython  
  make install DESTDIR="$pkgdir"
}

package_python-ipaplatform()
{
  arch=('any')
  depends=(
    "python-cffi"
    # "ipalib",  # circular dependency
    "python-ipapython"
    "python-pyasn1"
    "python-six"
  )
  cd freeipa-${pkgver}/ipaplatform
  make install DESTDIR="$pkgdir"
}

package_python-ipalib() {
  pkgdesc='Python libraries used by IPA'
  arch=('any')
  depends=(
    'python-gssapi>=1.2.0'
    'gnupg'
    'keyutils'
    'python-nss>=0.16'
    'python-cryptography>=1.4'
    'python-netaddr>=0.7.16'
    'sssd'
    'python-qrcode>=5.0.0'
    'python-pyasn1'
    'python-pyasn1-modules'
    'python-dateutil'
    'python-yubico>=1.2.3'
    'python-dbus'
    'python-setuptools'
    'python-six'
    'python-ldap'
    'python-dnspython>=1.15'
    'python-netifaces>=0.10.4'
    'python-pyusb'
    'python-ipapython')

  cd freeipa-${pkgver}

  install -D -m644 -t"$pkgdir"/usr/share/doc/$pkgname README.md \
          Contributors.txt

  cd ipalib
  make install DESTDIR="$pkgdir"

}

package_python-ipaclient() {
  pkgdesc='Python libraries used by IPA client'
  arch=('any')
  depends=(
    "python-ipalib=$pkgver-$pkgrel"
    "python-ipapython"
    "python-ipaplatform"
    'python-cryptography'
    'python-qrcode'
    'python-six'
  )

  cd freeipa-${pkgver}

  install -D -m644 -t"$pkgdir"/usr/share/doc/$pkgname README.md \
          Contributors.txt

  cd ipaclient
  make install DESTDIR="$pkgdir"
  install -Dm644 "$srcdir"/freeipa.conf.sysusers "$pkgdir"/usr/lib/sysusers.d/freeipa.conf
}


package_freeipa-client() {
  pkgdesc='IPA authentication for use on clients'
  depends=("python-ipaclient=$pkgver-$pkgrel"
           'python-augeas'
           'cyrus-sasl-gssapi'
           'ntp'
           'krb5'
           'authconfig'
           'curl>=7.21.7'
           'yp-tools'
           'xmlrpc-c>=1.27.4'
           'sssd>=1.14.0'
           'certmonger>=0.78'
           'nss'
           'bind-tools'
           'python-gssapi>=1.2.0'
           'nfsidmap'
          )


  cd freeipa-${pkgver}

  install -D -t"$pkgdir"/usr/share/libalpm/scripts \
          "$srcdir"/freeipa-client-update-sshd_config
  install -D -m644 -t"$pkgdir"/usr/share/libalpm/hooks \
          "$srcdir"/freeipa-client-update-sshd_config.hook \

          install -D -m644 -t"$pkgdir"/usr/share/doc/$pkgname README.md \
            Contributors.txt

  cd client
  make install DESTDIR="$pkgdir"
}

package_freeipa-server()
{
  pkgdesc="$_pkgdesc_base server"
  arch=('i686' 'x86_64')
  depends=(python-ipaclient
           python-ldap
           389-ds-base
           openldap
           nss
           krb5
           gssproxy
           sssd
          )


  cd freeipa-${pkgver}

  for dir in daemons init install ; do
    (
      cd "$dir"
      make install DESTDIR="$pkgdir"
    )
  done
}
